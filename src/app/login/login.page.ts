import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import { Vibration } from '@ionic-native/vibration/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string = " ";
  password: string = "";
  constructor(private fb: Facebook,private vibration: Vibration,public afAuth: AngularFireAuth, public router: Router,public alert: AlertController) { }

  ngOnInit() {
  }

  loginfb(){

    this.fb.login(['public_profile', 'user_friends', 'email'])
        .then((res: FacebookLoginResponse) => {
          this.showAlert('Error!', 'User not found');
          console.log('Logged into Facebook!', res)
        })
        .catch(e => console.log('Error logging into Facebook', e));

  }

  async login(){
    const {username, password} = this
    if(username == " "){
      // this.showAlert('Error!', 'Write username plekase');
      this.vibration.vibrate(5000);
      this.vibration.vibrate([200,100,200]);
    }else {

      try {
        const res = await this.afAuth.auth.signInWithEmailAndPassword(username , password);
        //this.presentLoadingWithOptions();
        this.router.navigate(['/feed']);
      } catch (err) {
        console.dir(err);
        this.showAlert("Error", err);
        if(err.code == "auth/invalid-email"){
          //console.log("User not found");
          this.showAlert('Error!', 'User not found');
        }
      }

    }

  }

  async showAlert(header: string, message: string) {
    const  alert = await this.alert.create({
      header,
      message,
      buttons: ['OK']
    });

    await alert.present();
  }



}
