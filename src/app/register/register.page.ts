import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  username = '';
  password = '';
  cpassword = '';

  constructor(public afAuth: AngularFireAuth, public alert: AlertController, public router: Router, public loadingController: LoadingController ) { }

  ngOnInit() {
  }

  async register() {
    const {username, password, cpassword} = this;
    if (password != cpassword) {
     this.showAlert('Error!', 'Passwords don\'t match');
     // console.log('password don\'t match');
    } else {
      try {
        const res = await this.afAuth.auth.createUserWithEmailAndPassword(username , password);

        this.presentLoadingWithOptions();
        // console.log(res);
        // this.showAlert('Success!', 'Welcome');
        this.router.navigate(['/login']);
      } catch (error) {
        // console.dir(error);
        this.showAlert('Error!', error.message);
      }
    }
  }

  async showAlert(header: string, message: string) {
    const  alert = await this.alert.create({
      header,
      message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 2000,
      message: 'Chargement...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }


}
