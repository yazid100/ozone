import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {Router} from '@angular/router';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})



export class FeedPage implements OnInit {

  constructor(public afAuth: AngularFireAuth, public router: Router) { }

  ngOnInit() {
  }


  logout() {
    this.afAuth.auth.signOut();
    this.router.navigate(["/home"]);
  }
  toLogin(){
    this.router.navigate(['/home']);
  }

}
